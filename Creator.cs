﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace Motivator
{
    public class Creator
    {
        private readonly FileManager _fileManager;
        private readonly GraphicManager _graphicManager;

        public Creator(FileManager fileManager, GraphicManager graphicManager)
        {
            _fileManager = fileManager;
            _graphicManager = graphicManager;
        }

        public string CreateMotivator()
        {
            var image = _fileManager.GetImage();
            var baseImage = _fileManager.GetBaseImage($@"{Directory.GetCurrentDirectory()}\motivator.jpg");
            var newImage = _graphicManager.NewImage(image, baseImage);
            var fileName = _fileManager.SaveImage(newImage);
            return fileName;
        }
    }
}
