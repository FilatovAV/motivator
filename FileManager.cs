﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;

namespace Motivator
{
    public class FileManager
    {
        private readonly string _fileName;

        public FileManager(string fileName)
        {
            this._fileName = fileName;
        }

        public Image GetBaseImage(string fileName)
        {
            var baseImage = Image.FromFile(fileName);
            return baseImage;
        }
        public Image GetImage()
        { 
            var image = Image.FromFile(_fileName);
            return image;
        }

        internal string SaveImage(Image image)
        {
            var randomFileName = $"{Path.GetFileNameWithoutExtension(Path.GetRandomFileName())}.jpg";
            image.Save(randomFileName, System.Drawing.Imaging.ImageFormat.Jpeg);
            return $@"{Directory.GetCurrentDirectory()}\{randomFileName}";
        }
    }
}
