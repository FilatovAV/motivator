﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Motivator
{
    public class GraphicManager
    {
        private readonly string message;
        private readonly Font _drawFont;
        private readonly SolidBrush _drawBrush;

        public GraphicManager(string message)
        {
            this.message = message;
            this._drawFont = new Font("Arial", 16, FontStyle.Bold);
            this._drawBrush = new SolidBrush(Color.Red);
        }

        internal Image NewImage(Image image, Image baseImage)
        {
            //var newImage = new Bitmap(100, 100);
            using (var graphics = Graphics.FromImage(baseImage))
            {
                var rect = new Rectangle() {Height = (baseImage.Size.Height / 100) * 80, Width = (baseImage.Size.Width / 100) * 80, X = baseImage.Size.Width / 10, Y = baseImage.Size.Height / 10 };
                graphics.DrawImage(image, rect);
                StringFormat sf = new StringFormat();
                sf.Alignment = StringAlignment.Center;
                sf.LineAlignment = StringAlignment.Center;
                graphics.DrawString(message, _drawFont, _drawBrush, new Point(baseImage.Size.Width / 2, (baseImage.Size.Width / 100) * 95), sf);
            }
            return baseImage;
        }
    }
}
