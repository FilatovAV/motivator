﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Motivator
{
    class Motivator
    {
        public string CreateMotivator(string image, string message)
        {
            var fileManager = new FileManager(image);
            var graphicManager = new GraphicManager(message);
            var creator = new Creator(fileManager, graphicManager);
            var result = creator.CreateMotivator();

            return result;
        }
    }
}
