﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace Motivator
{
    class Program
    {
        static void Main(string[] args)
        {
            var imageFileName = $@"{Directory.GetCurrentDirectory()}\i.jpg";
            var motivatorMessage = "Hello World!";
            var fileName = new Motivator().CreateMotivator(imageFileName, motivatorMessage);
            
            var argument = $"/select, {fileName}";
            Process.Start("explorer.exe", argument);
            Console.WriteLine(fileName);

            Console.ReadLine();
        }
    }
}
